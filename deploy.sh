#!/bin/bash

PATH=$PATH:$HOME/.local/bin

# Determine OS platform
UNAME=$(uname | tr "[:upper:]" "[:lower:]")
# If Linux, try to determine specific distribution
if [ "$UNAME" == "linux" ]; then
    # If available, use LSB to identify distribution
    if [ -f /etc/lsb-release -o -d /etc/lsb-release.d ]; then
        export DISTRO=$(lsb_release -i | cut -d: -f2 | sed s/'^\t'//)
    # Otherwise, use release info file
    else
        export DISTRO=$(ls -d /etc/[A-Za-z]*[_-][rv]e[lr]* | grep -v "lsb" | cut -d'/' -f3 | cut -d'-' -f1 | cut -d'_' -f1)
    fi
fi
# For everything else (or if above failed), just use generic identifier
[ "$DISTRO" == "" ] && export DISTRO=$UNAME
unset UNAME

# Bootstrap Ansible
if [ "$DISTRO" == "Ubuntu" ]; then
    if [ $(dpkg-query -W -f='${Status}' ansible 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        sudo apt-add-repository --yes --update ppa:ansible/ansible
        sudo apt-get install -y ansible
    else
        echo "Ansible already installed"
    fi

    if [ $(dpkg-query -W -f='${Status}' python-pip 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
        sudo apt-get install -y ansible
    fi

    # if hash mazer 2>/dev/null; then
    #     # mazer install crivetimihai.virtualization
    # else
    #     # pip install --upgrade mazer
    #     # mazer install crivetimihai.virtualization
    # fi

elif [ "$DISTRO" == "Fedora" ]; then
    if rpm -q ansible; then
        echo "Ansible already installed"
    else
        sudo dnf install -y ansible git
    fi
fi

if [ ! -f $HOME/.ansible.cfg ]; then
    touch $HOME/.ansible.cfg
    echo '[defaults]' >$HOME/.ansible.cfg
    echo 'remote_tmp     = /tmp/$USER/ansible' >>$HOME/.ansible.cfg
fi

# Run the playbook
ansible-galaxy install -r requirements.yml

./playbook.yml --ask-become