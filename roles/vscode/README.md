Role Name
=========

vscode

Requirements
------------


Role Variables
--------------


Dependencies
------------


Example Playbook
----------------

    - hosts: servers
      roles:
         - role: vscode

License
-------

MIT

Author Information
------------------

- [Eric Pfeiffer](https://www.linkedin.com/in/eric-pfeiffer-703910100/)
