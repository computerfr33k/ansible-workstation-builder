How to use
===

1. Clone this repository
2. Change the required vars in the `playbook.yml` file.
3. Execute `deploy.sh` which will start the provisioning process (this can be re-run)
4. When the `ask become` password prompt comes up, enter in your (sudo) password.

Variables
===

Configuration key|Description
-----------------|-----------
`sys_user`|this is the main system user we are building a profile and rolling out settings for; remember, this is a workstation!
`sys_user_email`|we can use this value to populate your email address where needed; i.e. your Git profile
`git_nickname`|self-explanatory